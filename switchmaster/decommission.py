import re

import requests

from .phabricator.lib import Client

def do_decommission(ticket):
    phab_client = Client.newFromCreds()
    phid = phab_client.lookupPhid(ticket)
    title = phab_client.taskDetails(phid).get('title')
    if not title:
        return { 'error': 'No ticket found' }
    dbanems = re.findall(r'\b((?:db|es|pc)\d{4})\b', title)
    dbanems = list(set(dbanems))
    if len(dbanems) != 1:
        return { 'error': 'Can not parse the db name' }
    db = dbanems[0]
    dc = 'eqiad' if db[2] == '1' else 'codfw'
    fqn = '{}.{}.wmnet'.format(db, dc)
    url = 'https://raw.githubusercontent.com/wikimedia/puppet/production/conftool-data/dbconfig-instance/instances.yaml'
    instances = [i.split(db)[1] for i in requests.get(url).text.split('\n') if db in i]
    if not instances:
        res = '[x] Remove host from dbctl (done or NA)\n'
    else:
        res = '[] Depool the host:\n```\n'
        if instances == ['']:
            res += 'sudo dbctl instance {db} depool\n'.format(db=db, ticket=ticket)
        else:
            for instance in instances:
                res += 'sudo dbctl instance {db}{instance} depool\n'.format(db=db, instance=instance, ticket=ticket)
        res += 'sudo dbctl config commit -m "Depool {db} {ticket}"\n```\n'.format(db=db, ticket=ticket)
        res += '[] Make puppet patch to remove it from dbctl \n'
        res += '[] Merge the puppet patch\n'
        res += '[] Run dbctl commit:\n'
        res += '```sudo dbctl config commit -m "Remove {db} from dbctl {ticket}"```\n'.format(db=db, ticket=ticket)
    res += '[] Make puppet patch cleaning the rest up (DO NOT merge)\n'
    res += '[] Run the decommissioning script:\n'
    res += '```sudo cookbook sre.hosts.decommission -t {ticket} {fqn}```\n'.format(ticket=ticket, fqn=fqn)
    res += '[] Merge the puppet patch\n'
    res += '[] Remove from zacillo\n'
    res += '  [] In IRC: `!log Removing {db} from zarcillo {ticket}`\n'.format(db=db, ticket=ticket)
    res += '  [] In cumin: sudo db-mysql db1215 -A zarcillo\n'
    res += """```set binlog_format='ROW';\ndelete from servers where hostname="{db}";\ndelete from instances where name like '{db}%';\ndelete from section_instances where instance like '{db}%';```\n""".format(db=db)
    res += '[] Remove host from orchestrator\n'
    res += '[] Update the task and hand it over to dcops\n'
    return { 'commands': res }