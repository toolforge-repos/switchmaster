import re
import sys

from .patch_maker.gerrit import GerritBot


class PuppetSwitchPatchMaker(GerritBot):
    def __init__(self, old_master, new_master, section, ticket):
        self.old_master = old_master
        self.new_master = new_master
        self.section = section
        self.ticket = ticket
        super().__init__(
            'operations/puppet',
            'mariadb: Promote {} to {} master\n\nBug:{}'.format(
                new_master,
                section,
                ticket),
            ticket)

    def changes(self):
        with open('hieradata/hosts/{}.yaml'.format(self.old_master), 'r') as f:
            content = f.read()
        new_content = []
        content = content.replace("profile::mariadb::mysql_role: 'master'", "").split('\n')
        done = False
        for line in content:
            if not line.strip():
                continue
            if line.startswith('#'):
                new_content.append(line)
                continue
            if not done:
                new_content.append('# candidate master for ' + self.section)
                done = True
            new_content.append(line)
        with open('hieradata/hosts/{}.yaml'.format(self.old_master), 'w') as f:
            f.write('\n'.join(new_content))
        
        with open('hieradata/hosts/{}.yaml'.format(self.new_master), 'r') as f:
            content = f.read().split('\n')
        new_content = []
        done = False
        for line in content:
            if line.startswith('#') and 'candidate master' in line:
                continue
            if line.startswith('#'):
                new_content.append(line)
                continue
            if not done:
                new_content.append("profile::mariadb::mysql_role: 'master'")
                done = True
            new_content.append(line)
        with open('hieradata/hosts/{}.yaml'.format(self.new_master), 'w') as f:
            f.write('\n'.join(new_content))

    def commit(self):
        self.check_call(['git', 'add', '.'])
        with open('.git/COMMIT_EDITMSG', 'w') as f:
            f.write(self.commit_message)
        self.check_call(['git', 'commit', '-F', '.git/COMMIT_EDITMSG'])
        self.check_call(self.build_push_command(
            {'repo': self.name, 'branch': 'production'}))

class DnsSwitchPatchMaker(GerritBot):
    def __init__(self, old_master, new_master, section, ticket):
        self.old_master = old_master
        self.new_master = new_master
        self.section = section
        self.ticket = ticket
        super().__init__(
            'operations/dns',
            'wmnet: Update {}-master alias\n\n{} will be the new {} master\n\nBug:{}'.format(
                section,
                new_master,
                section,
                ticket),
            ticket)

    def changes(self):
        with open('templates/wmnet', 'r') as f:
            content = f.read().split('\n')
        new_content = []
        for line in content:
            if not '{}-master'.format(self.section) in line:
                new_content.append(line)
                continue
            new_content.append(line.replace(self.old_master, self.new_master))
        with open('templates/wmnet', 'w') as f:
            f.write('\n'.join(new_content))

    def commit(self):
        self.check_call(['git', 'add', '.'])
        with open('.git/COMMIT_EDITMSG', 'w') as f:
            f.write(self.commit_message)
        self.check_call(['git', 'commit', '-F', '.git/COMMIT_EDITMSG'])
        self.check_call(self.build_push_command(
            {'repo': self.name, 'branch': 'master'}))

