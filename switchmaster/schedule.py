from .config import Config
from .patch_makers import PuppetSwitchPatchMaker, DnsSwitchPatchMaker
from .phabricator.lib import Client
import requests
import os
import yaml

dir = os.path.dirname(os.path.abspath(__file__))
def find_candidate_master(replicas):
    for replica in replicas:
        url = 'https://raw.githubusercontent.com/wikimedia/puppet/production/hieradata/hosts/{}.yaml'
        content = requests.get(url.format(replica.split(':')[0])).text
        print(content)
        if 'candidate master' in content:
            return replica
    raise RuntimeError


def find_primary_dc():
    yaml_text = requests.get('https://config-master.wikimedia.org/mediawiki.yaml').text
    return yaml.safe_load(yaml_text)['primary_dc']


def do_schedule(section, dc):
    affected_wikis = 'https://noc.wikimedia.org/conf/highlight.php?file=dblists/{}.dblist'.format(
        section) if section.startswith('s') else 'TO-DO'
    config = Config(dc)
    oldpri = config.get_section_master_for_dc(section, dc)
    newpri = find_candidate_master(config._get_replicas_for_dc(section, dc))
    taskid = 'TICKID'
    is_primary_dc = dc == find_primary_dc()

    if section.startswith('es'):
        if not is_primary_dc:
            task_template = 'task_template_es_secondary.txt'
        else:
            task_template = 'task_template_es_primary.txt'
    else:
        if not is_primary_dc:
            task_template = 'task_template_secondary.txt'
        else:
            task_template = 'task_template_primary.txt'
    with open(os.path.join(dir, task_template), 'r') as f:
        output = f.read()
    task_desc = output.format(
        section=section,
        oldpri=oldpri,
        newpri=newpri,
        taskid=taskid,
        dc=dc,
        affected_wikis=affected_wikis
    )
    
    phab_client = Client.newFromCreds()
    res = phab_client.createTicket(
        task_desc,
    ['PHID-PROJ-hwibeuyzizzy4xzunfsk'],
    'Switchover {} master ({} -> {})'.format(section, oldpri, newpri)
    )
    phid = res['object']['phid']
    taskid = 'T{}'.format(res['object']['id'])
    phab_client.setTaskDescription(phid, task_desc.replace('TICKID', taskid))
    
    
    maker = PuppetSwitchPatchMaker(
        oldpri,
        newpri,
        section,
        taskid
    )
    maker.run()

    if is_primary_dc:
        maker = DnsSwitchPatchMaker(
            oldpri,
            newpri,
            section,
            taskid
        )
        maker.run()

    return {
        'ticket': taskid
    }
