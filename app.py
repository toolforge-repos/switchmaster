import re
from flask import Flask, render_template, request
import os
import json

app = Flask(__name__)
dir = os.path.dirname(os.path.abspath(__file__))

from switchmaster.schedule import do_schedule
from switchmaster.decommission import do_decommission

@app.route("/")
def hello():
    return render_template('home.html')


@app.route("/schedule", methods=['GET'])
def schedule():
    return render_template('schedule.html')


@app.route("/schedule", methods=['POST'])
def schedule_post():
    with open(os.path.join(dir, 'acceptable_passwords.json'), 'r') as f:
        acceptable_passwords = json.loads(f.read())
    if request.form['password'].strip() not in acceptable_passwords:
        del acceptable_passwords
        return render_template('home.html')
    del acceptable_passwords
    section = request.form['section'].strip()
    dc = request.form['dc'].strip()
    if not re.findall(r'^\w\w?\d$', section) or dc not in ['eqiad', 'codfw']:
        return render_template('home.html')
    res = do_schedule(section, dc)
    return render_template(
        'schedule_done.html',
        section=section,
        dc=dc,
        ticket=res.get('ticket'),
        error=res.get('error'),
        warning=res.get('warning'))


@app.route("/decommission", methods=['GET'])
def decommission():
    return render_template('decommission.html')


@app.route("/decommission", methods=['POST'])
def decommission_post():
    ticket = request.form['ticket'].strip()
    if not re.findall(r'^T\d+$', ticket):
        return render_template('home.html')
    res = do_decommission(ticket)
    return render_template(
        'decommission_done.html',
        commands=res.get('commands'),
        error=res.get('error'))


if __name__ == "__main__":
    app.run(host='0.0.0.0')

